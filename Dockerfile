FROM python:3.10-alpine

COPY . /app
RUN pip install -r /app/requirements.txt

WORKDIR /app
ENV PYTHONPATH '/app/'

CMD ["python", "/app/gitlab_storage_analyzer.py"]

